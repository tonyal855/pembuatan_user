const {Sequelize} = require('sequelize');
const AgentModel = require('../model/Agent')
const PlayerModel = require('../model/Players')


var databse = 'games';
var usr = 'root';
var host = 'localhost';
var pass = '';

const sequelize = new Sequelize(databse, usr, pass, {
    host: host,
    dialect: 'mariadb' 
  });

const Agent = AgentModel(sequelize, Sequelize);
const Player = PlayerModel(sequelize, Sequelize);

// Agent.belongsTo(Player, {foreignKey: 'agent_id'});
Player.belongsTo(Agent, {foreignKey: 'agent_id'});


sequelize.sync()
.then(() => {
  console.log('table created')
})


module.exports = {Agent, Player}