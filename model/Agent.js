
module.exports = (sequelize, type) => {
    return sequelize.define('agents', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        parent_id: type.INTEGER,
        username:{
            type: type.STRING(35),
            unique: true
        },
        agent_code:{
            type: type.STRING(10),
            unique: true
        },
        password: {
            type: type.STRING(255)
        },
        api_key: {
            type: type.STRING(255),
            unique: true
        },
        email: {
            type: type.STRING(50),
            unique: true
        },
        phone: {
            type: type.STRING(12),
            unique: true
        },
        norek: {
            type: type.STRING(30),
        },
        bank_name: {
            type: type.STRING(35),
        },
        role: {
            type: type.ENUM('master', 'agent'),
        },

    })
}