
module.exports = (sequelize, type) =>{
    return sequelize.define('player',{
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        agent_id: {
            type: type.INTEGER,
        },
        user_id: {
            type: type.STRING(50),
            unique: true
        },
        displayName: {
            type: type.STRING(50),
        },
        badge: {
            type: type.ENUM('gold', 'standart'),
        },
        token: {
            type: type.STRING(255),
        },

    })
}