const express = require('express')
const db = require('./config/database')
const bodyParser = require('body-parser')
const {Agent, Player} = require('./config/database')
const router = express.Router();
const Controller = require('./controller/controller')


const app = express()
app.use(bodyParser.json())
const port = 3000

app.use(router)



router.get('/', Controller.hello);
router.post('/add_agent_master', Controller.add_agent_master)
router.get('/get_all_agent_master', Controller.get_all_agent_master)
router.get('/get_agent_master', Controller.get_agent_master)
router.post('/update_agent_master', Controller.update_agent_master)
router.post('/add_agent', Controller.add_agent)
router.get('/get_all_agent_by_parentid', Controller.get_all_agent_by_parentid)
router.get('/get_agent_by_parentid', Controller.get_agent_by_parentid)
router.post('/update_agent', Controller.update_agent)
router.post('/add_player', Controller.add_player)
router.get('/get_player', Controller.get_player)
router.post('/update_player', Controller.update_player)


app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})

