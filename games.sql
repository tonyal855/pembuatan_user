-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 27, 2021 at 11:14 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.2.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `games`
--

-- --------------------------------------------------------

--
-- Table structure for table `agents`
--

CREATE TABLE `agents` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `username` varchar(35) DEFAULT NULL,
  `agent_code` varchar(10) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `api_key` varchar(255) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(12) DEFAULT NULL,
  `norek` varchar(30) DEFAULT NULL,
  `bank_name` varchar(35) DEFAULT NULL,
  `role` enum('master','agent') DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `agents`
--

INSERT INTO `agents` (`id`, `parent_id`, `username`, `agent_code`, `password`, `api_key`, `email`, `phone`, `norek`, `bank_name`, `role`, `createdAt`, `updatedAt`) VALUES
(1, 1, 'tony_master', '002', 'sds1233', '2232ddww2asd', 'a@mail.com', '7892223', '6782221', 'bca', 'master', '2021-01-26 07:25:10', '2021-01-26 07:25:10'),
(5, 2, 'tonih_master', '002444', 'sds123333', '2232ddww224242asd', 'a@mails.com', '789222223', '67822221', 'mandiri', 'master', '2021-01-26 07:49:47', '2021-01-26 08:42:26'),
(6, 2, 'rida_agent', '42444', 'sds123333', 'x123ddww224242asd', 'ax@mails.com', '78921223', '67824121', 'bca', 'agent', '2021-01-26 08:13:16', '2021-01-26 09:27:03'),
(7, 2, 'roji', '44', 'sds123333', 'xe3ddww224242asd', 'ax2@mails.com', '71121223', '67824121', 'bca', 'agent', '2021-01-26 09:02:54', '2021-01-26 09:02:54'),
(8, 1, 'rojixx', '992', 'sds123333', 'xe3ddww224242asdtreq', 'axg@mails.com', '711212423', '678244121', 'bca', 'agent', '2021-01-26 09:05:51', '2021-01-26 09:05:51'),
(9, 1, 'roji123', '9922', 'sds123333', 'xe3ddww2242142asdtreq', 'axg@mais.com', '71121242223', '678244121', 'bca', 'agent', '2021-01-27 09:44:13', '2021-01-27 09:44:13'),
(10, 3, 'afrizal', '0022444', 'sds123333', '2232dd1ww224242asd', 'a@maixs.com', '7892221223', '67822221', 'bca', 'master', '2021-01-27 10:03:42', '2021-01-27 10:04:37'),
(11, 3, 'afrizal_asdx', '0022444424', 'sds123333', '2232dd1ww2224242asd', 'a@maixxs.com', '78922212123', '67822221', 'bca', 'agent', '2021-01-27 10:06:26', '2021-01-27 10:07:53');

-- --------------------------------------------------------

--
-- Table structure for table `players`
--

CREATE TABLE `players` (
  `id` int(11) NOT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `user_id` varchar(50) DEFAULT NULL,
  `displayName` varchar(50) DEFAULT NULL,
  `badge` enum('gold','standart') DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `players`
--

INSERT INTO `players` (`id`, `agent_id`, `user_id`, `displayName`, `badge`, `token`, `createdAt`, `updatedAt`) VALUES
(3, 6, '1232', 'tnyeasdf', 'gold', '2001', '2021-01-26 09:43:17', '2021-01-26 10:10:19'),
(4, 7, '14114', 'rojay', 'gold', '2041', '2021-01-26 09:43:52', '2021-01-26 09:43:52'),
(5, 8, '5114', 'rojayanto', 'gold', '20241', '2021-01-26 09:44:16', '2021-01-26 09:44:16'),
(7, 11, '214221', 'Afrijal', 'standart', '2003', '2021-01-27 10:09:54', '2021-01-27 10:11:47');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agents`
--
ALTER TABLE `agents`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `agent_code` (`agent_code`),
  ADD UNIQUE KEY `api_key` (`api_key`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `phone` (`phone`);

--
-- Indexes for table `players`
--
ALTER TABLE `players`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`),
  ADD KEY `agent_id` (`agent_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agents`
--
ALTER TABLE `agents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `players`
--
ALTER TABLE `players`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `players`
--
ALTER TABLE `players`
  ADD CONSTRAINT `players_ibfk_1` FOREIGN KEY (`agent_id`) REFERENCES `agents` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
