const {Agent, Player} = require('../config/database')

var Controller = {
    hello: hello,
    add_agent_master: add_agent_master,
    get_all_agent_master: get_all_agent_master,
    get_agent_master: get_agent_master,
    update_agent_master: update_agent_master,
    add_agent: add_agent,
    get_all_agent_by_parentid: get_all_agent_by_parentid,
    get_agent_by_parentid: get_agent_by_parentid,
    update_agent: update_agent,
    add_player: add_player,
    get_player: get_player,
    update_player: update_player
}
function hello(req, res){
    res.send('Add User App');
}

function add_agent_master(req, res){
    Agent.create(req.body)
      .then(agent => res.json(agent))
}

function get_all_agent_master(req, res){
    Agent.findAll({
    where: {
        role: 'master'
    }
  }).then(agentRes => res.json(agentRes))
}

function get_agent_master(req, res){
    Agent.findOne({
    where:{
        role: 'master'
    }
    }).then(agent => res.json(agent))
}

function update_agent_master(req, res){
    Agent.update(req.body, { where: { id: req.body.id } 
    }).then(agent => res.json(agent))
}

async function add_agent(req, res){
    const check_parent_id = await Agent.findOne({
    where:{
      parent_id: req.body.parent_id,
      role: 'master'
    }
  });
  if(check_parent_id){
    Agent.create(req.body)
    .then(agent => res.json(agent))
  }else{
      res.send("Master Agent Tidak ada")
  }
}

function get_all_agent_by_parentid(req, res){
    Agent.findAll({where: {
    parent_id: req.query.parent_id,
      role: 'agent'
    }
    }).then(agent => res.json(agent))
}

function get_agent_by_parentid(req, res){
  console.log(req.query.parent_id);
  Agent.findOne({where: {
    parent_id: req.query.parent_id,
    role: 'agent'
  }
  }).then(agent => res.json(agent))
}

async function update_agent(req, res){
    const result = await Agent.update(req.body, { where: { id: req.body.id, role: 'agent' } 
  })

  if(result == 1){
    res.send('succes update agent')
  }else{
    res.send('Gagal update agent')
  }
}

async function add_player(req, res){
    const getValid = await Agent.findOne({where:{
    id: req.body.agent_id,
    role: 'agent'
  }})
  if(getValid){
    Player.create(req.body)
    .then(player => res.json(player))

  }else{
    res.send('penambahan harus dengan agent')
  }
}

async function get_player(req, res){
    const getValid = await Agent.findOne({where:{
    id: req.query.agent_id,
    role: 'agent'
  }})
  if(getValid){
    Player.findOne({where: {
      agent_id: req.query.agent_id
    }
    }).then(player => res.json(player))
  }else{
    res.send('Tidak ada Akses')
  }
}

async function update_player(req, res){
    const getValid = await Agent.findOne({where:{
    id: req.body.agent_id,
    role: 'agent'
  }})
  if(getValid){
    Player.update(req.body, { where: { agent_id: req.body.agent_id }})
    .then(player => res.json(player))
  }else{
    res.send('Tidak ada Akses')
  }
}
module.exports = Controller;
